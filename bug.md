<details><summary>Detalhes:</summary>

Descrição um pouco mais elaborada sobre o que aconteceu, e abre espaço para mais detalhes do bug. EX: possível causa, formas de corrigir entre outros

## Detalhes do Bug

- 

</details>

<details><summary>Passos para reproduzir:</summary>

Descrição dos movimentos tomados para o bug acontecer. Ex: Sempre que clico em tal botão tal bug ocorre ou Ao clicar no botão 3x o bug vai ocorrer

## Passos para reproduzir

1. Passo 1
2. Passo 2
3. Passo 3
4. Passo 4
5. Passo 5

</details>

<details><summary>Evidencias:</summary>

Incluir nessa seção qualquer screenshot/vídeo relevante se necessário.

## Evidencias



</details>

<details><summary>Resultado esperado:</summary>

Descrição do resultado/comportamento esperado.

## Resultado esperado

- 

</details>

<details><summary>Resultado atual:</summary>

Descrição do resultado real que está ocorrendo.

## Resultado atual

-

</details>

<details><summary>Criticidade:</summary>

Selecione a criticidade do bug.

## Criticidade

- [ ] ALTO
- [ ] MEDIO
- [ ] BAIXO

## Impacto

- [ ] ALTO
- [ ] MEDIO
- [ ] BAIXO

</details>

<details><summary>Ambiente:</summary>

Informe o ambiente onde o bug foi encontrado. 

## Ambiente

-

</details>

<details><summary>Reportado por:</summary>

Informe o nome da pessoa que reportou o bug.

## Reportado por

-

</details>
