* **Dado** Passo 1
* **Quando** Passo 2
* **Então** Passo 3
    * **E** Passo 4

<details><summary>Pre-requisitos:</summary>

# Estar logado?
- [ ] Sim
- [ ] Nao

# Ter plano?
- [ ] Sim
- [ ] Nao

# Ter produto rodando?
- [ ] Sim
- [ ] Nao

# Observacoes extras:
- Caso exista algum prerequisito a mais que nao foi citado descrava aqui: